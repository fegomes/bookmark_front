app.controller("bookmarkCtrl",['$scope','$http',function($scope,$http){

  var user = JSON.parse(window.localStorage.getItem('user'));

  if (user!=null)
  {
    // token necessário para autenticar no acesso as APIs
    var headers={
      'headers':{
        'Authorization': 'Bearer'+window.localStorage.getItem('token')
      }
    };
  }

  $scope.editItem = function(item)
  {
    $scope.book = null;
    $scope.response = null;
    console.log(item);

    $scope.editBook = {
      description: item.description,
      url: item.url,
      id: item.id
    };
  }

  $scope.edit=function(editBook)
  {
    if (user!=null){
      editBook.user_id = user.id;
      console.log(editBook);
    }

    $http.put('http://localhost:8000/bookmarks/'+editBook.id,editBook,headers).then(function(response)
    {
      console.log('sucesso');
      console.log(response);

      $scope.response = {
        message: response.data
      };

      $scope.loadData();

    },function(response){
      console.log('error');
      console.log(response);
    });
  }

  $scope.add=function(book)
  {
    if (user!=null){
      book.user_id = user.id;
      console.log(book);    
    }

    $http.post('http://localhost:8000/bookmarks',book,headers).then(function(response)
    {
      console.log('sucesso');
      console.log(response);

      $scope.loadData();

    },function(response)
    {
      console.log('error');
      console.log(response);
    });
  }

  $scope.deleteBook = function(item) {
    $scope.removeBook = null;
    $scope.response = null;
    console.log(item);

    $scope.removeBook = {
      id: item.id
    }; 
  }

  $scope.delete=function(removeBook)
  {
    $http.delete('http://localhost:8000/bookmarks/'+removeBook.id,headers).then(function(response)
    {
      console.log('sucesso');
      console.log(response);
      
    $scope.response = {
      message: response.data
    };

    $scope.loadData();

    },function(response)
    {
      console.log('error');
      console.log(response);
    });
  }
  
  $scope.userInfo ={
    name: user.name,
    type: user.type
  };
  //atualiza a lista
  $scope.loadData = function()
  {
   //list bookmarks from all users to adm access
    if(user.type == 'adm')
    {
      $http.get('http://localhost:8000/bookmarks',headers).then(function(response)
      {
        console.log('sucesso');
        console.log(response);
        $scope.list = response.data;
        
      },function(response)
      {
        console.log('error');
        console.log(response);
      });
    }else{// list only bookmarks from this user
      $http.get('http://localhost:8000/bookmarks/'+user.id,headers).then(function(response)
      {
        console.log('sucesso');
        console.log(response);
        $scope.list = response.data;
      },function(response)
      {
        console.log('error');
        console.log(response);
      });
    }    
  }
  //atualiza a lista  
  $scope.loadData();
}]);