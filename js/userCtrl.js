admApp.controller("userCtrl",['$scope','$http',function($scope,$http){

  var user = JSON.parse(window.localStorage.getItem('user'));

  if (user!=null){
    // token necessário para autenticar no acesso as APIs
    var headers={
      'headers':{
        'Authorization': 'Bearer'+window.localStorage.getItem('token')
      }
  };
  }

  $scope.add=function(user)
  {
    $http.post('http://localhost:8000/users',user,headers).then(function(response){
      console.log('sucesso');
      console.log(response);

      $scope.loadData();

    },function(response){
      console.log('error');
      console.log(response);
    });
  }


  $scope.editItem = function(item) {
    $scope.editUser = null;
    $scope.response = null;
    console.log(item);

    $scope.editUser = {
      id: item.id,
      name: item.name,
      username: item.username,
      email: item.email,
      type: item.type,
      access: item.access
    };
  
  }


  $scope.edit=function(editUser)
  {
    console.log(editUser);
    $http.put('http://localhost:8000/users/'+editUser.id,editUser,headers).then(function(response){
      console.log('sucesso');
      console.log(response);
      
      $scope.response = {
        message: response.data
      };

      $scope.loadData();

    },function(response){
      console.log('error');
      console.log(response);
    });
  }

  $scope.deleteUser = function(item) {
    $scope.removeUser = null;
    $scope.response = null;
    console.log(item);

    $scope.removeUser = {
      id: item.id
    };
  
  }

  $scope.delete=function(removeUser)
  {
    console.log(removeUser.id);
    $http.delete('http://localhost:8000/users/'+removeUser.id,headers).then(function(response){
      console.log('sucesso');
      console.log(response);
      
      $scope.response = {
        message: response.data
      };
      $scope.loadData();

    },function(response){
      console.log('error');
      console.log(response);
    });
  }

  $scope.userInfo ={
    name: user.name,
    type: user.type
  };
  //atualiza a lista  
  $scope.loadData = function(){
    if (user!=null){
     
     //list all users to adm access
      if(user.type == 'adm'){
        $http.get('http://localhost:8000/users',headers).then(function(response){
          console.log('sucesso');
          console.log(response);
          $scope.list = response.data;
          
        },function(response){
          console.log('error');
          console.log(response);
        });
      }else{// redirect user not allowed.
        location.href='index.html';
      }       
    }
  } 
  //atualiza a lista    
  $scope.loadData();
}]);